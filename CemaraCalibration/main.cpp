#include "main.h"

// main
int main(int argc, char *argv[]){

	// aruco init
	if (MyAruco.Initialize()) return 0;
	window_width = MyAruco.camera_width;
	window_height = MyAruco.camera_height;

	// glutのコールバック関数
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(0, 200);
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("MainWindow");
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);

	// 初期化
	glClearColor(0.0f, 0.3f, 0.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);					// 両面

	// shader init
	InitGLProjectionMatrix(MyAruco.intrinsic, gl_pc);
	InitShader();

	// obj load
	if (!Objmesh.LoadFile("data\\texture.obj")){
		getchar();
		exit(0);
	}

	// main loop
	glutMainLoop();

	return 0;
}

// display
void display(void){

	// aruco tracking
	MyAruco.MainLoop();

	// 画面クリア
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// viewport
	glViewport(0, 0, window_width, window_height);

	//---------------------------------------------------------------
	// 撮影画像の描画
	//---------------------------------------------------------------
	// 一番うしろに描画したいので
	glDisable(GL_DEPTH_TEST);

	// shader set
	glUseProgram(imgProgram);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_RECTANGLE, imgTexId);

	// 撮影画像をＧＰＵにおくる
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB, window_width, window_height,
		0, GL_BGR, GL_UNSIGNED_BYTE, MyAruco.GetImage().data);


	// 図形の描画
	glNormal3d(0.0, 0.0, 1.0);
	glBegin(GL_QUADS);
	glTexCoord2d(0.0, 1.0); glVertex2d(-1.0, -1.0);
	glTexCoord2d(1.0, 1.0); glVertex2d(1.0, -1.0);
	glTexCoord2d(1.0, 0.0); glVertex2d(1.0, 1.0);
	glTexCoord2d(0.0, 0.0); glVertex2d(-1.0, 1.0);
	glEnd();

	// デプス有効
	glEnable(GL_DEPTH_TEST);
	//---------------------------------------------------------------
	// 推定変換行列からオブジェクトを描画
	//---------------------------------------------------------------
	// shader set
	glUseProgram(glProgram);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, glTexId);

	// 変換行列を読み込んでＧＰＵに転送
	GLfloat gl_m[16], gl_normal[9];
	MyAruco.GetMatrix(gl_m, 0);

	AsaTranslate(gl_m, 0, 0, Objmesh.GetSphere().radius);
	AsaRotate(gl_m, 90, 1, 0, 0);

	for (int yy = 0; yy < 3; yy++)
	for (int xx = 0; xx < 3; xx++)
		gl_normal[yy * 3 + xx] = gl_m[yy * 4 + xx];

	glUniformMatrix4fv(glGetUniformLocation(glProgram, "matM"), 1, GL_FALSE, gl_m);
	glUniformMatrix3fv(glGetUniformLocation(glProgram, "matNormal"), 1, GL_FALSE, gl_normal);

	for (int yy = 0; yy < 4; yy++){
		for (int xx = 0; xx < 4; xx++){
			cout << gl_m[yy * 4 + xx] << " ";
		}
		cout << endl;
	}

	// 描画
	Objmesh.Draw();
	//glutSolidCube(200);


	//---------------------------------------------------------------
	// ここまで
	//---------------------------------------------------------------

	// ダブルバッファリング
	glutSwapBuffers();

	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

}


// shader
void InitShader(){
	// glew初期化
	glewInit();

	// シェーダ読み込み
	MakeShader(glProgram, "simple.vert", "simple.frag");

	// texture
	glActiveTexture(GL_TEXTURE0);
	LoadGLTexture("data\\texture.png", glTexId, false);

	glUseProgram(glProgram);
	glUniform1i(glGetUniformLocation(glProgram, "tex1"), 0);
	glUniformMatrix4fv(glGetUniformLocation(glProgram, "matPC"), 1, GL_FALSE, gl_pc);

	// シェーダ読み込み
	MakeShader(imgProgram, "image.vert", "image.frag");

	// texture
	cv::Mat timg = MyAruco.GetImage().clone();
	if (timg.channels() == 1) cv::cvtColor(timg, timg, CV_GRAY2BGR);

	glActiveTexture(GL_TEXTURE2);
	glGenTextures(1, &imgTexId);
	glBindTexture(GL_TEXTURE_RECTANGLE, imgTexId);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB, window_width, window_height, 0, GL_BGR, GL_UNSIGNED_BYTE, timg.data);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	glUseProgram(imgProgram);
	glUniform1i(glGetUniformLocation(imgProgram, "image"), 2);
	glUniform1f(glGetUniformLocation(imgProgram, "height"), window_height + 0.5);

	// 解除
	glUseProgram(0);
}

//********************************************************
// OpenCV + OpenGL関連
//********************************************************
// gl用の投影行列
void InitGLProjectionMatrix(cv::Mat _intrinsic, GLfloat _m[]){

	double focalmax = 2000.0, focalmin = 10;
	double width = window_width, height = window_height;

	double fx = _intrinsic.at<double>(0, 0), fy = _intrinsic.at<double>(1, 1);
	double cx = _intrinsic.at<double>(0, 2), cy = height - _intrinsic.at<double>(1, 2);

	cv::Mat mP = cv::Mat::zeros(4, 4, CV_64FC1);
	mP.at<double>(0, 0) = (2.0 * fx / (width - 1));
	mP.at<double>(0, 1) = 0.0;
	mP.at<double>(0, 2) = -((2.0 * cx / (width - 1)) - 1.0);
	mP.at<double>(0, 3) = 0.0;

	mP.at<double>(1, 0) = 0.0;
	mP.at<double>(1, 1) = -(2.0 * fy / (height - 1));
	mP.at<double>(1, 2) = -((2.0 * cy / (height - 1)) - 1.0);
	mP.at<double>(1, 3) = 0.0;

	mP.at<double>(2, 0) = 0.0;
	mP.at<double>(2, 1) = 0.0;
	mP.at<double>(2, 2) = (focalmax + focalmin) / (focalmin - focalmax);
	mP.at<double>(2, 3) = 2.0 * focalmax * focalmin / (focalmin - focalmax);

	mP.at<double>(3, 0) = 0.0;
	mP.at<double>(3, 1) = 0.0;
	mP.at<double>(3, 2) = -1.0;
	mP.at<double>(3, 3) = 0.0;

	cv::Mat tntin = cv::Mat::eye(4, 4, CV_64FC1);
	//tntin.at<double>(0, 0) = -1.0;
	tntin.at<double>(2, 2) = -1.0;
	//tntin.at<double>(1, 1) = -1.0;
	mP = mP * tntin;

	for (int ii = 0; ii < 4; ii++)
	for (int jj = 0; jj < 4; jj++){
		_m[ii * 4 + jj] = mP.at<double>(jj, ii);
	}


}

// Glfloat 計算関数
void AsaTranslate(GLfloat _m[], double _x, double _y, double _z){
	for (int ii = 0; ii < 3; ii++)
		_m[12 + ii] += (_m[0 + ii] * _x + _m[4 + ii] * _y + _m[8 + ii] * _z);
}

void AsaRotate(GLfloat _m[], double _angle, double _x, double _y, double _z){
	_angle = _angle / 180.0*3.1415;
	float c = cos(_angle);
	float s = sin(_angle);

	GLfloat rot[16] = {
		_x*_x*(1.0 - c) + c, _y*_x*(1.0 - c) - _z*s, _x*_z*(1.0 - c) - _y*c, 0,
		_x*_y*(1.0 - c) - _z*s, _y*_y*(1.0 - c) + c, _y*_z*(1.0 - c) + _x*s, 0,
		_x*_z*(1.0 - c) + _y*s, _y*_z*(1.0 - c) - _x*s, _z*_z*(1.0 - c) + c, 0,
		0, 0, 0, 1
	};

	GLfloat tm[16];
	for (int ii = 0; ii < 16; ii++) tm[ii] = _m[ii];

	for (int yy = 0; yy < 3; yy++)
	for (int xx = 0; xx < 4; xx++){
		_m[xx * 4 + yy] = 0;
		for (int ii = 0; ii < 4; ii++){
			_m[xx * 4 + yy] += (tm[ii * 4 + yy] * rot[xx * 4 + ii]);
		}
	}
}

//********************************************************
// シェーダー初期化関連の関数
//********************************************************
// テクスチャの読み込み
void LoadGLTexture(string _fileName, GLuint &_texID, const bool _rectFlag){

	// 読み込み
	cv::Mat image = cv::imread(_fileName, -1);
	cv::flip(image, image, 0);
	if (image.channels() == 3){
		cv::cvtColor(image, image, CV_BGR2BGRA);
	}
	else if (image.channels() == 1){
		cv::cvtColor(image, image, CV_GRAY2BGRA);
	}

	// rect or?
	unsigned int texVersion;
	if (_rectFlag) texVersion = GL_TEXTURE_RECTANGLE;
	else texVersion = GL_TEXTURE_2D;

	if (_texID == -1){
		glGenTextures(1, &_texID);
		glBindTexture(texVersion, _texID);

		// テクスチャ画像はバイト単位に詰め込まれている 
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		//glTexParameteri(texVersion, GL_GENERATE_MIPMAP, GL_TRUE);

		// テクスチャの割り当て 
		glTexImage2D(texVersion, 0, GL_RGBA, image.cols, image.rows, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);

		// テクスチャを拡大・縮小する方法の指定 
		glTexParameteri(texVersion, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(texVersion, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		//glTexParameteri(texVersion, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		//glTexParameteri(texVersion, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		// テクスチャの繰り返し方法の指定 
		//glTexParameteri(texVersion, GL_TEXTURE_WRAP_S, GL_CLAMP);
		//glTexParameteri(texVersion, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(texVersion, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(texVersion, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		GLfloat an[3] = { 1.0, 1.0, 1.0 };
		glTexParameterfv(texVersion, GL_TEXTURE_BORDER_COLOR, an);

		// マテリアルの情報を使うかどうかの指定
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	}
	else{
		// bind
		glBindTexture(texVersion, _texID);

		// テクスチャの割り当て 
		glTexImage2D(texVersion, 0, GL_RGBA, image.cols, image.rows, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);
	}

	// テクスチャのバインド解除
	glBindTexture(texVersion, 0);
}

// シェーダープログラムの初期化
void MakeShader(GLuint &_gl2Program, string _vertName, string _fragName)
{
	cout << "shader compile (" << _vertName << ", " << _fragName << ") -> ";

	// シェーダプログラムのコンパイル／リンク結果を得る変数 
	GLint compiled, linked;

	GLuint vertShader;
	GLuint fragShader;

	// シェーダオブジェクトの作成 
	vertShader = glCreateShader(GL_VERTEX_SHADER);
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	// シェーダのソースプログラムの読み込み 
	if (ReadShaderSource(vertShader, _vertName.c_str())) { getchar(); exit(1); }
	if (ReadShaderSource(fragShader, _fragName.c_str())) { getchar(); exit(1); }

	// バーテックスシェーダのソースプログラムのコンパイル 
	glCompileShader(vertShader);
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &compiled);
	PrintShaderInfoLog(vertShader);
	if (compiled == GL_FALSE) {
		fprintf(stderr, "Compile error in vertex shader.\n");
		getchar();
		exit(1);
	}

	// フラグメントシェーダのソースプログラムのコンパイル 
	glCompileShader(fragShader);
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &compiled);
	PrintShaderInfoLog(fragShader);
	if (compiled == GL_FALSE) {
		fprintf(stderr, "Compile error in fragment shader.\n");
		getchar();
		exit(1);
	}

	// プログラムオブジェクトの作成 
	_gl2Program = glCreateProgram();

	// シェーダオブジェクトのシェーダプログラムへの登録 
	glAttachShader(_gl2Program, vertShader);
	glAttachShader(_gl2Program, fragShader);

	// シェーダオブジェクトの削除 
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	// シェーダプログラムのリンク 
	glLinkProgram(_gl2Program);
	glGetProgramiv(_gl2Program, GL_LINK_STATUS, &linked);
	PrintProgramInfoLog(_gl2Program);
	if (linked == GL_FALSE) {
		fprintf(stderr, "Link error.\n");
		getchar();
		exit(1);
	}
	cout << "おしり" << endl;
}

// シェーダーのソースプログラムをメモリに読み込む
int ReadShaderSource(GLuint _shader, const char *_file){
	FILE *fp;
	const GLchar *source;
	GLsizei length;
	int ret;

	// ファイルを開く 
	fopen_s(&fp, _file, "rb");
	if (fp == NULL) {
		perror(_file);
		return -1;
	}

	// ファイルの末尾に移動し現在位置（つまりファイルサイズ）を得る 
	fseek(fp, 0L, SEEK_END);
	length = ftell(fp);

	// ファイルサイズのメモリを確保 
	source = (GLchar *)malloc(length);
	if (source == NULL) {
		fprintf(stderr, "Could not allocate read buffer.\n");
		return -1;
	}

	// ファイルを先頭から読み込む 
	fseek(fp, 0L, SEEK_SET);
	ret = fread((void *)source, 1, length, fp) != (size_t)length;
	fclose(fp);

	// シェーダのソースプログラムのシェーダオブジェクトへの読み込み 
	if (ret)
		fprintf(stderr, "Could not read file: %s.\n", _file);
	else
		glShaderSource(_shader, 1, &source, &length);

	// 確保したメモリの開放 
	free((void *)source);

	return ret;
}

// シェーダの情報を表示する
void PrintShaderInfoLog(GLuint _shader){
	GLsizei bufSize;

	glGetShaderiv(_shader, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1) {
		GLchar *infoLog;

		infoLog = (GLchar *)malloc(bufSize);
		if (infoLog != NULL) {
			GLsizei length;

			glGetShaderInfoLog(_shader, bufSize, &length, infoLog);
			fprintf(stderr, "InfoLog:\n%s\n\n", infoLog);
			free(infoLog);
		}
		else
			fprintf(stderr, "Could not allocate InfoLog buffer.\n");
	}
}

// プログラムの情報を表示する
void PrintProgramInfoLog(GLuint _program){
	GLsizei bufSize;

	glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1) {
		GLchar *infoLog;

		infoLog = (GLchar *)malloc(bufSize);
		if (infoLog != NULL) {
			GLsizei length;

			glGetProgramInfoLog(_program, bufSize, &length, infoLog);
			fprintf(stderr, "InfoLog:\n%s\n\n", infoLog);
			free(infoLog);
		}
		else
			fprintf(stderr, "Could not allocate InfoLog buffer.\n");
	}
}


//********************************************************
// glutのコールバック関数
//********************************************************
void resize(int _w, int _h)
{
	glutPostRedisplay();
}

void idle(void)
{
	glutPostRedisplay();
}

void mouse(int _button, int _state, int _x, int _y)
{
	glutPostRedisplay();
}

void motion(int _x, int _y)
{

	glutPostRedisplay();
}

void keyboard(unsigned char _key, int _x, int _y)
{
	// ESC か q をタイプしたら終了
	if (_key == 'q' || _key == '\033'){
		cout << "おしり";
		exit(0);
	}

	glutPostRedisplay();
}
























