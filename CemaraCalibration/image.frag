#version 330
// shadertype=<glsl>
// frag

// テクスチャ
uniform sampler2DRect image;
uniform float height;

// フレームバッファに出力するデータ
layout (location = 0) out vec4 fc1; 

void main(void){
	fc1 = texture(image, vec2(gl_FragCoord.x, height - gl_FragCoord.y));
}