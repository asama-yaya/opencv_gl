#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <math.h>
#include <time.h>

using std::cout;
using std::endl;

using std::vector;
using std::string;
using std::to_string;
using std::stoi;
using std::stod;
using std::stoul;

using std::ifstream;
using std::ofstream;
using std::ios;

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定
// OpenGL --------------------------------------------------------------------------------------
#include <glew.h>
#include <glut.h>
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "glut32.lib")
// MyHeader-------------------------------------------------------------------------------------
#include "MyAruco.h"
#include "OBJLoader.h"

//**********************************************************************************************
// variable
//**********************************************************************************************
int window_width = 640;
int window_height = 480;

TMyAruco MyAruco;
OBJMESH Objmesh;

// projection matrix
GLfloat gl_pc[16];

// shader
GLuint glProgram, imgProgram;
GLuint glTexId = -1, imgTexId = -1;


//**********************************************************************************************
// Method
//**********************************************************************************************
// shader
void InitShader();
void InitGLProjectionMatrix(cv::Mat _intrinsic, GLfloat _m[]);

// Glfloat 計算関数
void AsaTranslate(GLfloat _m[], double _x, double _y, double _z);
void AsaRotate(GLfloat _m[], double _angle, double _x, double _y, double _z);

// shader 初期化関連
void LoadGLTexture(string _fileName, GLuint &_texID, const bool _rectFlag = false);
void MakeShader(GLuint &_gl2Program, string _vertName, string _fragName);
int ReadShaderSource(GLuint _shader, const char *_file);
void PrintShaderInfoLog(GLuint _shader);
void PrintProgramInfoLog(GLuint _program);

// glの関数
void display(void);
void resize(int _w, int _h);
void mouse(int _button, int _state, int _x, int _y);
void motion(int _x, int _y);
void keyboard(unsigned char _key, int _x, int _y);
void idle(void);